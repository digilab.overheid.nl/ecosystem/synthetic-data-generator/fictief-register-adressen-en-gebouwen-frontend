package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"strings"
	"text/template"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-frontend/backend"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-frontend/helpers"
)

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"formatTime": helpers.FormatTime,
		"formatDate": helpers.FormatDate,
		"formatYear": helpers.FormatYear,
		"title":      cases.Title(language.Dutch).String, // Note: modern equivalent of the deperacted strings.Title function
		"trim":       strings.TrimSpace,
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Addressess
	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the addresses from the backend

		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10"))
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		response := new(backend.Response[backend.Address])
		if err := backend.Request("GET", fmt.Sprintf("/addresses?%s", query.Encode()), nil, &response); err != nil {
			return fmt.Errorf("error fetching addresses: %v", err)
		}

		return c.Render("index", fiber.Map{"Addresses": response.Data, "Pagination": response.Pagination, "HasPagination": false})
	})

	addresses := app.Group("/addresses")

	addresses.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the address from the backend
		address := new(backend.Address)
		if err := backend.Request("GET", fmt.Sprintf("/addresses/%s", c.Params("id")), nil, address); err != nil {
			return fmt.Errorf("error fetching address: %v", err)
		}

		return c.Render("address-details", fiber.Map{"Address": address})
	})

	// Buildings
	buildings := app.Group("/buildings")

	buildings.Get("/", func(c *fiber.Ctx) error {
		// Fetch the buildings from the backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10"))
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		response := new(backend.Response[backend.Building])
		if err := backend.Request("GET", fmt.Sprintf("/buildings?%s", query.Encode()), nil, &response); err != nil {
			return fmt.Errorf("error fetching buildings: %v", err)
		}

		return c.Render("building-index", fiber.Map{"Buildings": response.Data, "Pagination": response.Pagination})
	})

	buildings.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the building from the backend
		building := new(backend.Building)
		if err := backend.Request("GET", fmt.Sprintf("/buildings/%s", c.Params("id")), nil, building); err != nil {
			return fmt.Errorf("error fetching building: %v", err)
		}

		return c.Render("building-details", fiber.Map{"Building": building})
	})

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
