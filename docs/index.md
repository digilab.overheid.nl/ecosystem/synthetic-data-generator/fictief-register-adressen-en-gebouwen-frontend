---
title : "Fictief Register Adressen en Gebouwen - frontend"
description: "Frontend to control and visualize Fictief Register Adressen en Gebouwen, part of the Synthentic Data Generator."
date: 2023-08-17T09:14:40+02:00
draft: false
toc: true
---

## Running locally

Clone this repo. Note: static files (css/scss/js/img) are stored in the `static` dir, but built into the `public` dir.

When running for the first time, make sure `pnmp` is installed (e.g. using `brew install pnpm`) and run `pnpm install`.

Run:

```sh
pnpm run dev
go run main.go -port 8081
```

This starts a web server locally on the specified port.
