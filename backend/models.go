package backend

import (
	"time"

	"github.com/google/uuid"
)

// IMPROVE: import models from gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen/pkg/model

type Municipality struct {
	Code string `json: code`
	Name string `json: name`
}

type Address struct {
	ID                  uuid.UUID    `json:"id"`
	Street              string       `json:"street"`
	HouseNumber         int32        `json:"houseNumber"`
	HouseNumberAddition string       `json:"houseNumberAddition,omitempty"`
	ZipCode             string       `json:"zipCode"`
	Municipality        Municipality `json:"municipality"`
	Purpose             string       `json:"purpose"`
	Surface             int32        `json:"surface"`
	Buildings           []Building   `json:"buildings"`
}

type AddressCreate struct {
	ID           uuid.UUID    `json:"id"`
	Street       string       `json:"street"`
	ZipCode      string       `json:"zipCode"`
	Municipality Municipality `json:"municipality"`
	Purpose      string       `json:"purpose"`
	Surface      int32        `json:"surface"`
}

type AddressUpdate struct {
	Street       string                      `json:"street"`
	ZipCode      string                      `json:"zipCode"`
	Municipality struct{ Code, Name string } `json:"municipality"`
	Purpose      string                      `json:"purpose"`
	Surface      int32                       `json:"surface"`
}

type Building struct {
	ID            uuid.UUID   `json:"id"`
	PlotID        uuid.UUID   `json:"plotId"`
	ConstructedAt time.Time   `json:"constructedAt"`
	Surface       int32       `json:"surface,omitempty"`
	Addresses     []Address   `json:"addresses"`
	Plots         []uuid.UUID `json:"plots"`
}

type BuildingCreate struct {
	ID            uuid.UUID `json:"id"`
	PlotID        uuid.UUID `json:"plotId"`
	ConstructedAt time.Time `json:"constructedAt"`
	Surface       int32     `json:"surface,omitempty"`
}

type BuildingUpdate struct {
	PlotID        uuid.UUID `json:"plotId"`
	ConstructedAt time.Time `json:"constructedAt"`
	Surface       int32     `json:"surface,omitempty"`
}

type BuildingAttach struct {
	Addresses []uuid.UUID `json:"addresses"`
}

type Pagination struct {
	FirstID *uuid.UUID `json:"firstId"`
	LastID  *uuid.UUID `json:"lastId"`
	PerPage int        `json:"perPage"`
}

type Response[T any] struct {
	Data       []T         `json:"data"`
	Pagination *Pagination `json:"pagination"`
}
