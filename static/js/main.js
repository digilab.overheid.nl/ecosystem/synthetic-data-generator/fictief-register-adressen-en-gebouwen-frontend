// Imports
import './htmx.js'; // Note: imported this way, since some plugins need the global `htmx` variable, which requires the use of an extra file according to https://htmx.org/docs/#webpack (which indeed seems to be the case)

import { TempusDominus } from '@eonasdan/tempus-dominus';

const datepickerOptions = {
  display: {
    theme: 'light',
    icons: {
      type: 'sprites',
      time: '/img/feather-sprite.svg#clock',
      date: '/img/feather-sprite.svg#calendar',
      up: '/img/feather-sprite.svg#arrow-up',
      down: '/img/feather-sprite.svg#arrow-down',
      previous: '/img/feather-sprite.svg#arrow-left',
      next: '/img/feather-sprite.svg#arrow-right',
    },
    buttons: {
      today: false,
      clear: false,
      close: false,
    }
  },
  localization: {
    // today: 'Go to today',
    // clear: 'Clear selection',
    // close: 'Close the picker',
    // selectMonth: 'Select Month',
    // previousMonth: 'Previous Month',
    // nextMonth: 'Next Month',
    // selectYear: 'Select Year',
    // previousYear: 'Previous Year',
    // nextYear: 'Next Year',
    // selectDecade: 'Select Decade',
    // previousDecade: 'Previous Decade',
    // nextDecade: 'Next Decade',
    // previousCentury: 'Previous Century',
    // nextCentury: 'Next Century',
    // pickHour: 'Pick Hour',
    // incrementHour: 'Increment Hour',
    // decrementHour: 'Decrement Hour',
    // pickMinute: 'Pick Minute',
    // incrementMinute: 'Increment Minute',
    // decrementMinute: 'Decrement Minute',
    // pickSecond: 'Pick Second',
    // incrementSecond: 'Increment Second',
    // decrementSecond: 'Decrement Second',
    // toggleMeridiem: 'Toggle Meridiem',
    // selectTime: 'Select Time',
    // selectDate: 'Select Date',
    dayViewHeaderFormat: { month: 'long', year: 'numeric' },
    locale: 'default',
    startOfTheWeek: 1,
    hourCycle: 'h23',
    dateFormats: {
      LTS: 'HH:mm:ss',
      LT: 'HH:mm',
      L: 'dd-MM-yyyy',
      LL: 'd MMMM, yyyy',
      LLL: 'd MMMM, yyyy HH:mm',
      LLLL: 'dddd, d MMMM, yyyy HH:mm'
    },
  }
};

// ESDateTimePicker consists of a text input with datetimepicker
class ESDateTimePicker extends HTMLElement {
  constructor() {
    // Always call super first in the constructor
    super();

    // Set properties
    this.value = this.getAttribute('value') || '';

    // Create a wrapper div
    const wrap = document.createElement('div');
    wrap.className = 'relative';

    // Create an icon div element
    const icon = document.createElement('div');
    icon.className = 'absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none';
    icon.innerHTML = `<svg aria-hidden="true" class="w-5 h-5 text-gray-500" fill="currentColor" viewBox="0 0 20 20"
        xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd"
          d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
          clip-rule="evenodd"></path>
      </svg>`;

    // Create an input element
    const input = document.createElement('input');
    input.setAttribute('id', this.getAttribute('id'));
    this.removeAttribute('id'); // Remove the `id` attribute from the Web Component, so the browser knows which element to focus when a label for it is clicked
    input.setAttribute('name', this.getAttribute('name'));
    input.setAttribute('type', 'text');
    input.setAttribute('value', this.value);
    input.setAttribute('placeholder', this.getAttribute('placeholder'));
    input.setAttribute('autocomplete', 'off');
    input.className = 'bg-white border border-gray-300 text-gray-800 text-sm rounded-lg focus:outline-none focus:ring-1 focus:ring-sky-500 focus:border-sky-500 block w-full p-2.5 pl-10 shadow-sm placeholder:text-gray-400 ' + this.getAttribute('extra-input-classes');

    // Append and insert the elements
    wrap.replaceChildren(icon, input);
    this.replaceChildren(wrap);

    // Initialize the datetimepicker
    new TempusDominus(input, datepickerOptions);

    // Do not let the change event on the input propagate to the Web Component
    input.addEventListener('change', (e) => {
      e.stopPropagation();
    })
    
    // On hide, fire the change event on the Web Component
    input.addEventListener('hide.td', () => {
      this.value = input.value;
      this.dispatchEvent(new Event('change'));
    })
  }
}

// Define the web components
if ('customElements' in window) {
  customElements.define('es-datetimepicker', ESDateTimePicker);
}
